import { InputModule } from './../../shared/components/input/input.module';
import { ButtonModule } from './../../shared/components/button/button.module';
import { ProductModule } from './product/product.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';



@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ProductModule,
    ButtonModule,
    InputModule
  ],
  exports: [
    ProductsComponent
  ]
})
export class ProductsModule { }
