import { InputModule } from './../../shared/components/input/input.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';



@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    InputModule
  ],
  exports: [
    CartComponent
  ]
})
export class CartModule { }
